/**
 * Created by admin on 09/18/15.
 */

public class SumFct {
        public static int perimeter(int n) {
            int[] S = new int[n+1];
            S[0] = 1;
            S[1] = 1;
            int sum = 2;
            for (int i = 2; i <= n; i++){
                S[i] = S[i-1] + S[i-2];
                sum += S[i];
            }
            return sum;
        }
        public static void main(String[] args){
            int n = Integer.parseInt(args[0]);
            int sum = perimeter(n);
            System.out.print(sum);
        }
}
